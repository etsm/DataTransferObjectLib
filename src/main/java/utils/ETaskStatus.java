package utils;

public enum ETaskStatus {
    WORK_IN_PROGRESS,
    RESOLVE,
    READY_FOR_WORK
}
