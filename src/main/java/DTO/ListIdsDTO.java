package DTO;

import java.util.List;
import java.util.Objects;

public class ListIdsDTO {
    private List<Long> ids;

    public ListIdsDTO(List<Long> ids) {
        this.ids = ids;
    }

    public ListIdsDTO() {
    }

    public List<Long> getIds() {
        return ids;
    }

    public void setIds(List<Long> ids) {
        this.ids = ids;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ListIdsDTO)) return false;
        ListIdsDTO that = (ListIdsDTO) o;
        return Objects.equals(ids, that.ids);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ids);
    }
}
