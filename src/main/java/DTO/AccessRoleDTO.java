package DTO;



import builders.AccessRoleDTOBuilder;

import java.util.Objects;
import java.util.Set;

public class AccessRoleDTO {
    public static AccessRoleDTOBuilder builder(){
        return new AccessRoleDTOBuilder();
    }

    private Long id;
    private String name;
    private Set<EmployeeDTO> employees;

    public AccessRoleDTO(Long id, String name, Set<EmployeeDTO> employees) {
        this.id = id;
        this.name = name;
        this.employees = employees;
    }

    public AccessRoleDTO(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public AccessRoleDTO(String name) {

        this.name = name;
    }

    public AccessRoleDTO() {
    }

    public Long getId() {
        return id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<EmployeeDTO> getEmployees() {
        return employees;
    }

    public void setEmployees(Set<EmployeeDTO> employees) {
        this.employees = employees;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccessRoleDTO that = (AccessRoleDTO) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name) && Objects.equals(employees, that.employees);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, employees);
    }
}
