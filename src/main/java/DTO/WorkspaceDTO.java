package DTO;

import builders.WorkspaceDTOBuilder;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.Objects;
import java.util.Set;

@JsonIdentityInfo(generator= ObjectIdGenerators.IntSequenceGenerator.class, property="@id_workspace")
public class WorkspaceDTO {
    public static WorkspaceDTOBuilder builder(){
        return new WorkspaceDTOBuilder();
    }

    private Long id;
    private Long departmentId;
    private Long taskCounter;
    private String identifier;
    private Set<TaskDTO> taskList;
    private String description;
    private String name;

    public WorkspaceDTO(Long id, Long departmentId, Long taskCounter, String identifier, Set<TaskDTO> taskSet, String description) {
        this.id = id;
        this.departmentId = departmentId;
        this.taskCounter = taskCounter;
        this.identifier = identifier;
        this.taskList = taskSet;
        this.description = description;
    }

    public WorkspaceDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public Set<TaskDTO> getTaskList() {
        return taskList;
    }

    public void setTaskList(Set<TaskDTO> taskList) {
        this.taskList = taskList;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getTaskCounter() {
        return taskCounter;
    }

    public void setTaskCounter(Long taskCounter) {
        this.taskCounter = taskCounter;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WorkspaceDTO)) return false;
        WorkspaceDTO that = (WorkspaceDTO) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name) && Objects.equals(identifier, that.identifier);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, identifier);
    }
}
