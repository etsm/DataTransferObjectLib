package DTO;

import builders.EmployeeDTOBuilder;

import java.util.*;

/**
 * firstName!
 * secondName!
 * patronymic
 * accessToSystem!
 * department!
 * accessRoles!
 * email!
 */
public class EmployeeDTO {

    public static EmployeeDTOBuilder builder(){
        return new EmployeeDTOBuilder();
    }

    private Long id;
    private String displayName;
    private String firstName;
    private String secondName;
    private String patronymic = null;
    private Boolean accessToSystem = false;
    private String email;
    private String login;
    private Long loginId;
    private DepartmentDTO department;
    private Set<AccessRoleDTO> accessRoleSet = new HashSet<>();

    public EmployeeDTO() {
    }

    public EmployeeDTO(EmployeeDTO dto) {
        this.id = dto.id;
        this.displayName = dto.displayName;
        this.firstName = dto.firstName;
        this.secondName = dto.secondName;
        this.patronymic = dto.patronymic;
        this.accessToSystem = dto.accessToSystem;
        this.email = dto.email;
        this.login = dto.login;
        this.loginId = dto.loginId;
        this.department = dto.department;
        this.accessRoleSet = dto.accessRoleSet;
    }

    public Long getId() {
        return id;
    }

    public Long getLoginId() {
        return loginId;
    }

    public void setLoginId(Long loginId) {
        this.loginId = loginId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public Boolean getAccessToSystem() {
        return accessToSystem;
    }

    public String getEmail() {
        return email;
    }

    public DepartmentDTO getDepartment() {
        return department;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public void setAccessToSystem(Boolean accessToSystem) {
        this.accessToSystem = accessToSystem;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setDepartment(DepartmentDTO department) {
        this.department = department;
    }

    public Set<AccessRoleDTO> getAccessRoleSet() {
        return accessRoleSet;
    }

    public void setAccessRoleSet(Set<AccessRoleDTO> accessRoleSet) {
        this.accessRoleSet = accessRoleSet;
    }

    public String getDisplayName() {
        return firstName + " " + secondName + " " + patronymic;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EmployeeDTO)) return false;
        EmployeeDTO that = (EmployeeDTO) o;
        return Objects.equals(id, that.id) && Objects.equals(displayName, that.displayName) && Objects.equals(firstName, that.firstName) && Objects.equals(secondName, that.secondName) && Objects.equals(patronymic, that.patronymic) && Objects.equals(accessToSystem, that.accessToSystem) && Objects.equals(email, that.email) && Objects.equals(login, that.login) && Objects.equals(loginId, that.loginId) && Objects.equals(department, that.department) && Objects.equals(accessRoleSet, that.accessRoleSet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, displayName, firstName, secondName, patronymic, accessToSystem, email, login, loginId, department, accessRoleSet);
    }
}