package DTO;

import builders.TaskDTOBuilder;
import com.fasterxml.jackson.annotation.*;
import utils.ETaskStatus;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

@JsonIdentityInfo(generator= ObjectIdGenerators.IntSequenceGenerator.class, property="@id_task")
public class TaskDTO implements Comparable<TaskDTO> {

    public static TaskDTOBuilder builder() {
        return new TaskDTOBuilder();
    }

    /**
     * id задачи
     */
    private Long id;

    /**
     * id исполнителя
     */
    private Long executorId;

    /**
     * id автора
     */
    private Long authorId;

    /**
     * идентификатор задачи
     */
    private String taskIdentifier;

    /**
     * проект/пространство, к которому принадлежит задача
     */
    private WorkspaceDTO workspace;

    /**
     * Название задачи
     */
    private String title;

    /**
     * статус задачи
     */
    private ETaskStatus status;

    /**
     * описание задачи
     */
    private String description;

    /**
     * время создание задачи
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private LocalDateTime createTime;
    /**
     * время первого смены статуса c READY_FOR_WORK на WORK_IN_PROGRESS
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private LocalDateTime startTime;

    /**
     * время смены статуса c WORK_IN_PROGRESS на RESOLVE
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private LocalDateTime endTime;

    /**
     * первичная оценка времения выполнения задачи
     */
    private Duration initialEstimateOfTaskExecutionTime;

    /**
     * затраенное время на выполнение задачи
     */
    private Duration elapsedTaskExecutionTime;

    /**
     * оставшееся время выполнения задачи
     */
    private Duration timeRemainingForTheTask;

//    @JsonManagedReference
    private Set<TaskRelationDTO> taskRelations = new TreeSet<>();

    public TaskDTO(Long id, Long executorId, Long authorId, String taskIdentifier, WorkspaceDTO workspace, String title, ETaskStatus status, String description, LocalDateTime createTime, LocalDateTime startTime, LocalDateTime endTime, Duration initialEstimateOfTaskExecutionTime, Duration elapsedTaskExecutionTime, Duration timeRemainingForTheTask, Set<TaskRelationDTO> taskRelations) {
        this.id = id;
        this.executorId = executorId;
        this.authorId = authorId;
        this.taskIdentifier = taskIdentifier;
        this.workspace = workspace;
        this.title = title;
        this.status = status;
        this.description = description;
        this.createTime = createTime;
        this.startTime = startTime;
        this.endTime = endTime;
        this.initialEstimateOfTaskExecutionTime = initialEstimateOfTaskExecutionTime;
        this.elapsedTaskExecutionTime = elapsedTaskExecutionTime;
        this.timeRemainingForTheTask = timeRemainingForTheTask;
        this.taskRelations = taskRelations;
    }

    public TaskDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getExecutorId() {
        return executorId;
    }

    public void setExecutorId(Long executorId) {
        this.executorId = executorId;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public String getTaskIdentifier() {
        return taskIdentifier;
    }

    public void setTaskIdentifier(String taskIdentifier) {
        this.taskIdentifier = taskIdentifier;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ETaskStatus getStatus() {
        return status;
    }

    public void setStatus(ETaskStatus status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public Duration getInitialEstimateOfTaskExecutionTime() {
        return initialEstimateOfTaskExecutionTime;
    }

    public void setInitialEstimateOfTaskExecutionTime(Duration initialEstimateOfTaskExecutionTime) {
        this.initialEstimateOfTaskExecutionTime = initialEstimateOfTaskExecutionTime;
    }

    public Duration getElapsedTaskExecutionTime() {
        return elapsedTaskExecutionTime;
    }

    public void setElapsedTaskExecutionTime(Duration elapsedTaskExecutionTime) {
        this.elapsedTaskExecutionTime = elapsedTaskExecutionTime;
    }

    public Duration getTimeRemainingForTheTask() {
        return timeRemainingForTheTask;
    }

    public void setTimeRemainingForTheTask(Duration timeRemainingForTheTask) {
        this.timeRemainingForTheTask = timeRemainingForTheTask;
    }

    public WorkspaceDTO getWorkspace() {
        return workspace;
    }

    public void setWorkspace(WorkspaceDTO workspace) {
        this.workspace = workspace;
    }

    public Set<TaskRelationDTO> getTaskRelations() {
        return taskRelations;
    }

    public void setTaskRelations(Set<TaskRelationDTO> taskRelations) {
        this.taskRelations = taskRelations;
    }

    @Override
    public int compareTo(TaskDTO taskDTO) {
        return this.id.compareTo(taskDTO.id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TaskDTO)) return false;
        TaskDTO taskDTO = (TaskDTO) o;
        return Objects.equals(id, taskDTO.id) && Objects.equals(executorId, taskDTO.executorId) && Objects.equals(authorId, taskDTO.authorId) && Objects.equals(taskIdentifier, taskDTO.taskIdentifier) && Objects.equals(title, taskDTO.title) && status == taskDTO.status && Objects.equals(description, taskDTO.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, executorId, authorId, taskIdentifier, title, status, description);
    }
}
