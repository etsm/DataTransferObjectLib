package DTO;

import builders.DepartmentDTOBuilder;

public class DepartmentDTO  {

    public static DepartmentDTOBuilder builder() {
        return new DepartmentDTOBuilder();
    }
    
    private Long id;
    private String name;
    private EmployeeDTO headOfDepartment;

    public DepartmentDTO(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public DepartmentDTO(Long id) {
        this.id = id;
    }

    public DepartmentDTO() {
        
    }

    public EmployeeDTO getHeadOfDepartment() {
        return headOfDepartment;
    }

    public void setHeadOfDepartment(EmployeeDTO headOfDepartment) {
        this.headOfDepartment = headOfDepartment;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
