package DTO;

import java.util.Objects;

public class ComplexPasswordDTO {
    private Long employeeId;
    private String currentPassword;
    private String newPassword;


    public ComplexPasswordDTO(Long employeeId, String currentPassword, String newPassword) {
        this.employeeId = employeeId;
        this.currentPassword = currentPassword;
        this.newPassword = newPassword;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ComplexPasswordDTO that = (ComplexPasswordDTO) o;
        return Objects.equals(employeeId, that.employeeId) && Objects.equals(currentPassword, that.currentPassword) && Objects.equals(newPassword, that.newPassword);
    }

    @Override
    public int hashCode() {
        return Objects.hash(employeeId, currentPassword, newPassword);
    }
}
