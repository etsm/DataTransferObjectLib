package DTO;

import builders.EmployeeDTOBuilder;
import builders.EmployeeWithTasksDTOBuilder;

import java.util.List;

public class EmployeeWithTasksDTO extends EmployeeDTO {

    public static EmployeeWithTasksDTOBuilder builder(){
        return new EmployeeWithTasksDTOBuilder();
    }

    private List<TaskDTO> taskList;

    public EmployeeWithTasksDTO(List<TaskDTO> taskList) {
        super();
        this.taskList = taskList;
    }

    public EmployeeWithTasksDTO() {
        super();
    }

    public EmployeeWithTasksDTO(EmployeeDTO employeeDTO) {
        super(employeeDTO);
    }


    public List<TaskDTO> getTaskList() {
        return taskList;
    }

    public void setTaskList(List<TaskDTO> taskList  ){
        this.taskList = taskList;
    }

}
