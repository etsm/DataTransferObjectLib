package DTO;


import builders.LoginDTOBuilder;

import java.util.List;
import java.util.Objects;
import java.util.Set;

public class LoginDTO extends EmployeeDTO {
    public static LoginDTOBuilder builder(){
        return new LoginDTOBuilder();
    }

    private String password;

    public LoginDTO() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}