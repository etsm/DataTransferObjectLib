package DTO;

import builders.TaskRelationDTOBuilder;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import utils.ERelationType;

@JsonIdentityInfo(generator= ObjectIdGenerators.IntSequenceGenerator.class, property="@id_relation")
public class TaskRelationDTO implements Comparable<TaskRelationDTO> {
    public static TaskRelationDTOBuilder builder(){
        return new TaskRelationDTOBuilder();
    }

    private Long id;
    private TaskDTO task;
    private ERelationType type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TaskDTO getTask() {
        return task;
    }

    public void setTask(TaskDTO task) {
        this.task = task;
    }

    public ERelationType getType() {
        return type;
    }

    public void setType(ERelationType type) {
        this.type = type;
    }

    @Override
    public int compareTo(TaskRelationDTO relatedTask) {
        return id.compareTo(relatedTask.id);
    }
}