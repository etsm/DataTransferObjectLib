package builders;

import DTO.TaskDTO;
import DTO.WorkspaceDTO;

import java.util.Set;

public class WorkspaceDTOBuilder {
    private final WorkspaceDTO workspaceDTO;

    public WorkspaceDTOBuilder() {
        this.workspaceDTO = new WorkspaceDTO();
    }

    public WorkspaceDTOBuilder id(Long id){
        this.workspaceDTO.setId(id);
        return this;
    }

    public WorkspaceDTOBuilder name(String name) {
        this.workspaceDTO.setName(name);
        return this;
    }

    public WorkspaceDTOBuilder departmentId(Long departmentId){
        this.workspaceDTO.setDepartmentId(departmentId);
        return this;
    }

    public WorkspaceDTOBuilder taskCounter(Long counter) {
        this.workspaceDTO.setTaskCounter(counter);
        return this;
    }

    public WorkspaceDTOBuilder identifier(String identifier) {
        this.workspaceDTO.setIdentifier(identifier);
        return this;
    }

    public WorkspaceDTOBuilder description(String description) {
        this.workspaceDTO.setDescription(description);
        return this;
    }

    public WorkspaceDTOBuilder taskList(Set<TaskDTO> dtos){
        this.workspaceDTO.setTaskList(dtos);
        return this;
    }

    public WorkspaceDTO build(){
        return this.workspaceDTO;
    }
}
