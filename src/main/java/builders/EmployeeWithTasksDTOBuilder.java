package builders;

import DTO.*;

import java.util.List;
import java.util.Set;

public class EmployeeWithTasksDTOBuilder extends EmployeeDTOBuilder {

    private final EmployeeWithTasksDTO employeeWithTasksDTO;

    public EmployeeWithTasksDTOBuilder() {
       super();
        employeeWithTasksDTO = new EmployeeWithTasksDTO();
    }

    public EmployeeWithTasksDTOBuilder taskList(List<TaskDTO> taskDTOList) {
        employeeWithTasksDTO.setTaskList(taskDTOList);
        return this;
    }

    @Override
    public EmployeeWithTasksDTOBuilder id(Long id) {
        super.id(id);
        return this;
    }

    @Override
    public EmployeeWithTasksDTOBuilder displayName(String displayName) {
        super.displayName(displayName);
        return this;
    }

    @Override
    public EmployeeWithTasksDTOBuilder firstName(String firstName) {
        super.firstName(firstName);
        return this;
    }

    @Override
    public EmployeeWithTasksDTOBuilder secondName(String secondName) {
        super.secondName(secondName);
        return this;
    }

    @Override
    public EmployeeWithTasksDTOBuilder patronymic(String patronymic) {
        super.patronymic(patronymic);
        return this;
    }

    @Override
    public EmployeeWithTasksDTOBuilder accessToSystem(Boolean accessToSystem) {
        super.accessToSystem(accessToSystem);
        return this;
    }

    @Override
    public EmployeeWithTasksDTOBuilder email(String email) {
        super.email(email);
        return this;
    }

    @Override
    public EmployeeWithTasksDTOBuilder department(DepartmentDTO department) {
        super.department(department);
        return this;
    }

    @Override
    public EmployeeWithTasksDTOBuilder accessRoleSet(Set<AccessRoleDTO> accessRole) {
        super.accessRoleSet(accessRole);
        return this;
    }

    @Override
    public EmployeeWithTasksDTOBuilder loginId(Long loginId) {
        super.loginId(loginId);
        return this;
    }

    @Override
    public EmployeeWithTasksDTOBuilder login(String login) {
        super.login(login);
        return this;
    }

    public EmployeeWithTasksDTO build() {
        EmployeeDTO employeeDTO = super.build();
        EmployeeWithTasksDTO employeeWithTasksDTO = new EmployeeWithTasksDTO(employeeDTO);
        employeeWithTasksDTO.setTaskList(this.employeeWithTasksDTO.getTaskList());
        return employeeWithTasksDTO;
    }
}
