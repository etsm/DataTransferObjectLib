package builders;

import DTO.TaskRelationDTO;
import DTO.TaskDTO;
import utils.ERelationType;

public class TaskRelationDTOBuilder {
    private final TaskRelationDTO dto;

    public TaskRelationDTOBuilder() {
        this.dto = new TaskRelationDTO();
    }

    public TaskRelationDTOBuilder id(Long id) {
        dto.setId(id);
        return this;
    }

    public TaskRelationDTOBuilder task(TaskDTO taskDTO) {
        dto.setTask(taskDTO);
        return this;
    }

    public TaskRelationDTOBuilder type(ERelationType type) {
        dto.setType(type);
        return this;
    }

    public TaskRelationDTO build(){
        return dto;
    }
}
