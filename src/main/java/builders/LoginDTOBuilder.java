package builders;

import DTO.*;

import java.util.Set;

public class LoginDTOBuilder extends  EmployeeDTOBuilder {
    private final LoginDTO loginDTO;

    public LoginDTOBuilder(){
        this.loginDTO = new LoginDTO();
    }

    public LoginDTOBuilder password(String password) {
        this.loginDTO.setPassword(password);
        return this;
    }

    public LoginDTOBuilder id(Long id) {
        this.loginDTO.setId(id);
        return this;
    }

    public LoginDTOBuilder displayName(String displayName) {
        this.loginDTO.setDisplayName(displayName);
        return this;
    }

    public LoginDTOBuilder firstName(String firstName) {
        this.loginDTO.setFirstName(firstName);
        return this;
    }

    public LoginDTOBuilder secondName(String secondName) {
        this.loginDTO.setSecondName(secondName);
        return this;
    }

    public LoginDTOBuilder patronymic(String patronymic) {
        this.loginDTO.setPatronymic(patronymic);
        return this;
    }

    public LoginDTOBuilder accessToSystem(Boolean accessToSystem) {
        this.loginDTO.setAccessToSystem(accessToSystem);
        return this;
    }

    public LoginDTOBuilder email(String email) {
        this.loginDTO.setEmail(email);
        return this;
    }

    public LoginDTOBuilder department(DepartmentDTO department) {
        this.loginDTO.setDepartment(department);
        return this;
    }

    public LoginDTOBuilder accessRole(Set<AccessRoleDTO> accessRole) {
        this.loginDTO.setAccessRoleSet(accessRole);
        return this;
    }

    public LoginDTOBuilder login(String login) {
        this.loginDTO.setLogin(login);
        return this;
    }

    public LoginDTO build(){
        return this.loginDTO;
    }
}
