package builders;

import DTO.DepartmentDTO;
import DTO.EmployeeDTO;

public class DepartmentDTOBuilder {
    private final DepartmentDTO departmentDTO;

    public DepartmentDTOBuilder(){
        this.departmentDTO = new DepartmentDTO();
    }


    public DepartmentDTOBuilder name(String name) {
        this.departmentDTO.setName(name);

        return this;
    }

    public DepartmentDTOBuilder id(Long id) {
        this.departmentDTO.setId(id);
        return this;

    }

    public DepartmentDTOBuilder headOfDepartment(EmployeeDTO employeeDTO) {
        this.departmentDTO.setHeadOfDepartment(employeeDTO);
        return this;
    }

    public DepartmentDTO build() {
        return this.departmentDTO;
    }
}
