package builders;

import DTO.AccessRoleDTO;
import DTO.EmployeeDTO;

import java.util.Set;

public class AccessRoleDTOBuilder {

    private final AccessRoleDTO accessRoleDTO;

    public AccessRoleDTOBuilder(){
        this.accessRoleDTO = new AccessRoleDTO();
    }

    public AccessRoleDTOBuilder id(Long id) {
        this.accessRoleDTO.setId(id);
        return this;
    }

    public AccessRoleDTOBuilder name(String name) {
        this.accessRoleDTO.setName(name);
        return this;
    }

    public AccessRoleDTOBuilder employees(Set<EmployeeDTO> dtoSet) {
        this.accessRoleDTO.setEmployees(dtoSet);
        return this;
    }

    public AccessRoleDTO build(){
        return this.accessRoleDTO;
    }
}
