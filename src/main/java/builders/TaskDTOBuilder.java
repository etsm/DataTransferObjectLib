package builders;

import DTO.TaskRelationDTO;
import DTO.TaskDTO;
import DTO.WorkspaceDTO;
import utils.ETaskStatus;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Set;

public class TaskDTOBuilder {
    private final TaskDTO taskDTO;

    public TaskDTOBuilder(){
        this.taskDTO = new TaskDTO();
    }

    public TaskDTOBuilder id(Long id) {
        this.taskDTO.setId(id);
        return this;
    }

    public TaskDTOBuilder executorId(Long executorId) {
        this.taskDTO.setExecutorId(executorId);
        return this;
    }

    public TaskDTOBuilder authorId(Long authorId) {
        this.taskDTO.setAuthorId(authorId);
        return this;
    }

    public TaskDTOBuilder taskIdentifier(String taskIdentifier) {
        this.taskDTO.setTaskIdentifier(taskIdentifier);
        return this;
    }

    public TaskDTOBuilder title(String title) {
        this.taskDTO.setTitle(title);
        return this;
    }

    public TaskDTOBuilder description(String description) {
        this.taskDTO.setDescription(description);
        return this;
    }

    public TaskDTOBuilder workspace(WorkspaceDTO workspace) {
        this.taskDTO.setWorkspace(workspace);
        return this;
    }

    public TaskDTOBuilder status(ETaskStatus status) {
        this.taskDTO.setStatus(status);
        return this;
    }

    public TaskDTOBuilder createTime(LocalDateTime createTime) {
        this.taskDTO.setCreateTime(createTime);
        return this;
    }

    public TaskDTOBuilder startTime(LocalDateTime startTime) {
        this.taskDTO.setStartTime(startTime);
        return this;
    }

    public TaskDTOBuilder endTime(LocalDateTime endTime) {
        this.taskDTO.setEndTime(endTime);
        return this;
    }

    public TaskDTOBuilder initialEstimateOfTaskExecutionTime(Duration initialEstimateOfTaskExecutionTime) {
        this.taskDTO.setInitialEstimateOfTaskExecutionTime(initialEstimateOfTaskExecutionTime);
        return this;
    }

    public TaskDTOBuilder elapsedTaskExecutionTime(Duration elapsedTaskExecutionTime) {
        this.taskDTO.setElapsedTaskExecutionTime(elapsedTaskExecutionTime);
        return this;
    }

    public TaskDTOBuilder timeRemainingForTheTask(Duration timeRemainingForTheTask) {
        this.taskDTO.setTimeRemainingForTheTask(timeRemainingForTheTask);
        return this;
    }

    public TaskDTOBuilder taskRelations(Set<TaskRelationDTO> taskRelations){
        this.taskDTO.setTaskRelations(taskRelations);
        return this;
    }



    public TaskDTO build(){
        return this.taskDTO;
    }
}
