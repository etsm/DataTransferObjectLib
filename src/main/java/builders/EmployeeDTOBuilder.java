package builders;

import DTO.AccessRoleDTO;
import DTO.DepartmentDTO;
import DTO.EmployeeDTO;
import DTO.EmployeeWithTasksDTO;

import java.util.Set;

public class EmployeeDTOBuilder {

    protected EmployeeDTO employeeDTO;

    public EmployeeDTOBuilder() {
        this.employeeDTO = new EmployeeDTO();
    }
    
    public EmployeeDTOBuilder id(Long id) {
        this.employeeDTO.setId(id);
        return this;
    }

    
    public EmployeeDTOBuilder displayName(String displayName) {
        this.employeeDTO.setDisplayName(displayName);
        return this;
    }
    
    public EmployeeDTOBuilder firstName(String firstName) {
        this.employeeDTO.setFirstName(firstName);
        return this;
    }

    
    public EmployeeDTOBuilder secondName(String secondName) {
        this.employeeDTO.setSecondName(secondName);
        return this;
    }

    
    public EmployeeDTOBuilder patronymic(String patronymic) {
        this.employeeDTO.setPatronymic(patronymic);
        return this;
    }

    
    public EmployeeDTOBuilder accessToSystem(Boolean accessToSystem) {
        this.employeeDTO.setAccessToSystem(accessToSystem);
        return this;
    }

    
    public EmployeeDTOBuilder email(String email) {
        this.employeeDTO.setEmail(email);
        return this;
    }

    
    public EmployeeDTOBuilder department(DepartmentDTO department) {
        this.employeeDTO.setDepartment(department);
        return this;
    }


    public EmployeeDTOBuilder accessRoleSet(Set<AccessRoleDTO> accessRole) {
        this.employeeDTO.setAccessRoleSet(accessRole);
        return this;
    }

    public EmployeeDTOBuilder loginId(Long loginId) {
        this.employeeDTO.setLoginId(loginId);
        return this;
    }

    public EmployeeDTOBuilder login(String login) {
        this.employeeDTO.setLogin(login);
        return this;
    }

    public EmployeeDTO build(){
        return this.employeeDTO;
    }
}
